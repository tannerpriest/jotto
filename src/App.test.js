import App from './App';
import { queryByTitleAttr, storeFactory } from '../test/testUtils';
import { render } from '@testing-library/react';

jest.mock('./actions')

import { getSecretWord as mockgetSecretWord } from "./actions"
import { Provider } from 'react-redux';

const setup = (initialState = {
    secretWord: 'party',
    success: false,
    guessedWords: []
}) => {
    const store = storeFactory()
    // @ts-ignore
    const wrapper = render(<Provider store={store}><App {...initialState} /></Provider>)

    return wrapper
}

describe('render', () => {

    test('renders app component', () => {
        const component = setup()
        const app = queryByTitleAttr(component, "component-app")
        expect(app).toBeTruthy()
    });

})

describe('get secret word', () => {
    beforeEach(() => {
        // @ts-ignore
        mockgetSecretWord.mockClear()
    })
    test('should get secret word on app mount', () => {
        setup()
        expect(mockgetSecretWord).toHaveBeenCalledTimes(1)
    })
    test('should not run getScretWord on app update', () => {
        const { rerender } = setup()
        // @ts-ignore
        mockgetSecretWord.mockClear()
        const state = {
            secretWord: 'shoe',
            success: false,
            guessedWords: [{ guessedWord: "agile", letterMatchCount: 1 }],
        }
        const store = storeFactory(state)
        rerender(<Provider store={store}>
            <App />
        </Provider>)
        expect(mockgetSecretWord).toHaveBeenCalledTimes(0)
    })

})
