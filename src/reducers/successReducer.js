import { actionTypes } from "../actions";

const defaultState = false

const success = (state = defaultState, action) => {
    switch (action.type) {
        case actionTypes.CORRECT_GUESS:
            return true
        case actionTypes.RESET_GAME:
            return defaultState
        default:
            return state
    }
}

export default success
