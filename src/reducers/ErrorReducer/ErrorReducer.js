import { actionTypes } from "../../actions";

const defaultState = false

const errorReducer = (state = defaultState, action) => {
    switch (action.type) {
        case actionTypes.NEW_ERROR:
            return true
        default:
            return state
    }
}

export default errorReducer