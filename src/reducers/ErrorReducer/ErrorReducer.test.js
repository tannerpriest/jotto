
import { actionTypes } from "../../actions"
import errorReducer from "./ErrorReducer"

const buttonAction = { type: actionTypes.NEW_ERROR }

describe('returned state', () => {
    test('should return default false state', () => {
        const newState = errorReducer(undefined, {})
        expect(newState).toBe(false)
    })
    test('should return default false state when action.type unknown', () => {
        const newState = errorReducer(undefined, {})
        expect(newState).toBe(false)
    })
    test('should return true state when payload is true', () => {
        const newState = errorReducer(undefined, buttonAction)
        expect(newState).toBe(true)
    })
})
