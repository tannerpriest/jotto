import { combineReducers } from "redux";
import success from "./successReducer";
import guessedWords from "./guessedWordsReducer";
import secretWord from "./secretWordReducer";
import newWordButtonVisible from "./NewWordButtonVisible";
import gaveUp from "./GaveUpReducer"
import errorReducer from "./ErrorReducer/ErrorReducer";
/** 
* To keep consistant witht the intergration tests, whenever a new reducer is added to redux state, update this object with default state of those reducers, to prevent failed tests that look for default state. 
* This object can be used in an expectedTest argument using the spread operator, then the updated state afterwords
* const expectedState = {
    ...expectedDefaultState,
    success: true,
    guessedWords: [{...}]
}
*/
export const expectedDefaultState = {
    success: success(undefined, {}),
    guessedWords: guessedWords(undefined, {}),
    secretWord: secretWord(undefined, {}),
    newWordButtonVisible: newWordButtonVisible(undefined, {}),
    gaveUp: gaveUp(undefined, {}),
    errors: errorReducer(undefined, {}),
}


export default combineReducers({
    success,
    guessedWords,
    secretWord,
    newWordButtonVisible,
    gaveUp,
    errors: errorReducer
})

