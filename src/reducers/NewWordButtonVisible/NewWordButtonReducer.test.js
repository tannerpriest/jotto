
import { actionTypes } from "../../actions"
import newWordButtonVisible from "./NewWordButtonVisibleReducer"

const buttonAction = { type: actionTypes.NEW_WORD_BUTTON_VISABLE }

describe('returned state', () => {
    test('should return default false state', () => {
        const newState = newWordButtonVisible(undefined, {})
        expect(newState).toBe(false)
    })
    test('should return default false state when action.type unknown', () => {
        const newState = newWordButtonVisible(undefined, {
            ...buttonAction,
            payload: false
        })
        expect(newState).toBe(false)
    })
    test('should return true state when payload is true', () => {
        const newState = newWordButtonVisible(undefined, {
            ...buttonAction,
            payload: true
        })
        expect(newState).toBe(true)
    })

})
