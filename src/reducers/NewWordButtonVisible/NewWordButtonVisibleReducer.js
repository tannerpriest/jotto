import { actionTypes } from "../../actions";

const defaultState = false

const newWordButtonVisible = (state = defaultState, action) => {
    switch (action.type) {
        case actionTypes.NEW_WORD_BUTTON_VISABLE:
            return action.payload
        case actionTypes.RESET_GAME:
            return defaultState
        default:
            return state
    }
}

export default newWordButtonVisible