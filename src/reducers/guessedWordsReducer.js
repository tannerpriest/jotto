import { actionTypes } from "../actions"

const defaultState = []

const guessedWords = (state = defaultState, action) => {
    switch (action.type) {
        case actionTypes.GUESS_WORD:
            return [...state, action.payload]
        case actionTypes.RESET_GAME:
            return defaultState
        default:
            return state
    }
}

export default guessedWords