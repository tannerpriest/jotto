import { actionTypes } from "../../actions";

const defaultState = false

const gaveUpReducer = (state = defaultState, action) => {
    switch (action.type) {
        case actionTypes.GAVE_UP:
            return true
        case actionTypes.RESET_GAME:
            return defaultState
        default:
            return state
    }
}

export default gaveUpReducer