
import { actionTypes } from "../../actions"
import gaveUpReducer from "./GaveUpReducer"

const buttonAction = { type: actionTypes.GAVE_UP }

describe('returned state', () => {
    test('should return default false state', () => {
        const newState = gaveUpReducer(undefined, {})
        expect(newState).toEqual(false)
    })
    test('should return default false state when action.type unknown', () => {
        const newState = gaveUpReducer(undefined, { type: "" })
        expect(newState).toEqual(false)
    })
    test('should return true state when action is GAVE_UP', () => {
        const newState = gaveUpReducer(undefined, buttonAction)
        expect(newState).toEqual(true)
    })
    test('should return false state when action is reset game', () => {
        const newState = gaveUpReducer(undefined, {
            type: actionTypes.RESET_GAME
        })
        expect(newState).toEqual(false)
    })
})
