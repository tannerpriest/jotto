import { createStore, applyMiddleware, compose } from "redux";
import ReduxThunk from "redux-thunk"
import rootReducer from "./reducers"

export const middleWares = [ReduxThunk]
// @ts-ignore
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, /* preloadedState, */ composeEnhancers(
    applyMiddleware(...middleWares)
));

export default store