import React from "react"
import { render, RenderResult } from "@testing-library/react"
import { fireEvent } from "@testing-library/dom";

import App from "./App"


import { queryByTitleAttr, queryAllTitleAttr, storeFactory } from "../test/testUtils"
import userEvent from "@testing-library/user-event"

import { Provider } from "react-redux"

/** 
 * Create wrapper with specified initial conditions,
 * the submit a guessed word of 'train'
 * @param {object} state -  Inital State conditions
 * @returns {RenderResult} - react-testing-library element
 */

jest.mock('./actions')


const setup = (state = {}) => {
    const store = storeFactory(state)
    const wrapper = render(<Provider store={store}><App {...state} /></Provider>)

    // add value to input box

    const inputBox = queryByTitleAttr(wrapper, "input-box")

    // userEvent.type(inputBox, "train")
    fireEvent.change(inputBox, { target: { value: "train" } })

    // simulate click on submit

    const submitButton = queryByTitleAttr(wrapper, "submit-button")

    userEvent.click(submitButton)

    return wrapper
}

describe('no words guessed', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = setup({
            secretWord: 'party',
            success: false,
            guessedWords: []
        })
    })
    test('creates guessed words table with is empty', () => {
        const GuessedWordRows = queryAllTitleAttr(wrapper, "guessed-word")
        expect(GuessedWordRows).toHaveLength(1)
    })
    test('instructions are displayed', () => {
        const gameInstructions = queryByTitleAttr(wrapper, "component-instructions")
        // expect(gameInstructions).toBeTruthy()
    })

    test('input box is displayed', () => {
        const inputBox = queryByTitleAttr(wrapper, "input-box")
        expect(inputBox).toBeTruthy()
    })
    test('submit button is displayed', () => {
        const submitButton = queryByTitleAttr(wrapper, "submit-button")
        expect(submitButton).toBeTruthy()
    })
    test('congrats message is hidden', () => {
        const congratsMessage = queryByTitleAttr(wrapper, "congrats-message")
        expect(congratsMessage).toBeFalsy()
    })
    test('creates guessed words table with 1 row', async () => {
        const GuessedWordRows = await queryAllTitleAttr(wrapper, "guessed-word")
        expect(GuessedWordRows).toHaveLength(1)
    })
})

describe('some words guessed', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = setup({
            secretWord: 'party',
            success: false,
            guessedWords: [{ guessedWord: "agile", letterMatchCount: 1 }]
        })
    })

    test('creates guessed words table with 2 rows', async () => {
        const GuessedWordRows = await queryAllTitleAttr(wrapper, "guessed-word")
        expect(GuessedWordRows).toHaveLength(2)
    })

})

describe('if secret word has been guessed', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = setup({
            secretWord: 'party',
            success: false,
            guessedWords: [{ guessedWord: "agile", letterMatchCount: 1 }]
        })

        const inputBox = queryByTitleAttr(wrapper, "input-box")

        fireEvent.change(inputBox, { target: { value: "party" } })

        const submitButton = queryByTitleAttr(wrapper, "submit-button")

        userEvent.click(submitButton)

    })

    test('creates guessed words table with 3 rows', async () => {
        const GuessedWordRows = await queryAllTitleAttr(wrapper, "guessed-word")
        expect(GuessedWordRows).toHaveLength(3)
    })
    test('should display congrats message', () => {
        const congrats = queryByTitleAttr(wrapper, "component-congrats")
        expect(congrats.innerHTML.length).toBeGreaterThan(0)
    })
    test('should not display input component', () => {

        const inputBox = queryByTitleAttr(wrapper, "input-box")

        expect(inputBox).toBeNull()

        // simulate click on submit

        const submitButton = queryByTitleAttr(wrapper, "submit-button")

        expect(submitButton).toBeNull()
    })

})


