import axios from "axios";
import { getLetterMatchCount } from "../helpers"

export const actionTypes = {
    CORRECT_GUESS: "CORRECT_GUESS",
    GUESS_WORD: "GUESS_WORD",
    SET_SECRET_WORD: "SET_SECRET_WORD",
    RESET_GAME: "RESET_GAME",
    NEW_WORD_BUTTON_VISABLE: "NEW_WORD_BUTTON_VISABLE",
    GAVE_UP: "GAVE_UP",
    NEW_ERROR: "NEW_ERROR",
}

export const guessWord = (guessedWord) => {
    return (dispatch, getState) => {
        const secretWord = getState().secretWord
        const letterMatchCount = getLetterMatchCount(guessedWord, secretWord)
        dispatch({
            type: actionTypes.GUESS_WORD,
            payload: { guessedWord, letterMatchCount }
        })
        if (guessedWord === secretWord) {
            dispatch({ type: actionTypes.CORRECT_GUESS })
            dispatch({ type: actionTypes.NEW_WORD_BUTTON_VISABLE, payload: true })
        }
    }
}

export const getSecretWord = () => {
    return function (dispatch) {
        return axios.get("http://localhost:3030").then(response => dispatch({
            type: actionTypes.SET_SECRET_WORD,
            payload: response.data
        })).catch(err => {
            dispatch({
                type: actionTypes.NEW_ERROR,
            })
        })
    }
}

export const resetGame = () => {
    return (dispatch) => {
        return axios.get("http://localhost:3030").then(response => {
            dispatch({
                type: actionTypes.RESET_GAME,
                payload: null
            })
            return dispatch({
                type: actionTypes.SET_SECRET_WORD,
                payload: response.data
            })
        }).catch(err => {
            dispatch({
                type: actionTypes.NEW_ERROR,
            })
        })
    }
}

export const giveUpAction = () => {
    return (dispatch) => {
        dispatch({
            type: actionTypes.GAVE_UP
        })
        dispatch({ type: actionTypes.NEW_WORD_BUTTON_VISABLE, payload: true })
    }
}