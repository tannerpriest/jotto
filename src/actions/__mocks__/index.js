module.exports = {
    ...jest.requireActual('..'),
    __esModule: true,
    getSecretWord: jest.fn().mockReturnValue({ type: "mock" }),
    resetGame: jest.fn().mockReturnValue({ type: "mock" }),
    giveUpAction: jest.fn().mockReturnValue({ type: "mock" }),
}