import moxios from "moxios"
import { storeFactory } from "../../test/testUtils"
import { expectedDefaultState } from "../reducers"
import { getSecretWord, giveUpAction, resetGame } from "./"


describe('get secret word', () => {
    beforeEach(() => {
        moxios.install()
    })
    afterEach(() => {
        moxios.uninstall()
    })
    test('should get secret word with getSecretWord', () => {
        const store = storeFactory()
        moxios.wait(() => {
            // listens for an axios request being sent out, and instead of the localhost responding, moxios responses instead
            const request = moxios.requests.mostRecent()
            request.respondWith({
                status: 200,
                response: "party"
            })
        })
        // @ts-ignore
        return store.dispatch(getSecretWord()).then((word) => {
            const newState = store.getState()
            expect(newState.secretWord).toBe("party")
        })
    })
    test('should get secret word with resetGame', () => {
        const store = storeFactory()
        moxios.wait(() => {
            // listens for an axios request being sent out, and instead of the localhost responding, moxios responses instead
            const request = moxios.requests.mostRecent()
            request.respondWith({
                status: 200,
                response: "great"
            })
        })
        // @ts-ignore
        return store.dispatch(resetGame()).then((word) => {
            const newState = store.getState()
            expect(newState.secretWord).toBe("great")
        })
    })
})

describe('giveUpAction', () => {
    test('should return correct state', () => {
        const store = storeFactory()
        // @ts-ignore
        store.dispatch(giveUpAction())
        const newState = store.getState()
        const expectedState = {
            ...expectedDefaultState,
            gaveUp: true,
            newWordButtonVisible: true
        }
        expect(newState).toEqual(expectedState)
    })
})
