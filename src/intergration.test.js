import moxios from "moxios";
import { storeFactory } from "../test/testUtils";

import { getSecretWord, guessWord, resetGame } from "./actions"
import { expectedDefaultState } from "./reducers";


describe('guessWord action dispatcher', () => {
    const secretWord = 'party'
    const unsuccessfulGuess = 'train'

    describe('no guessed words', () => {
        let store;
        const initialState = { secretWord }

        beforeEach(() => {
            store = storeFactory(initialState)
        })

        test('should update state correctly for unsuccessful guess', () => {
            store.dispatch(guessWord(unsuccessfulGuess))
            const expectedState = {
                ...expectedDefaultState,
                ...initialState,
                success: false,
                guessedWords: [
                    {
                        letterMatchCount: 3,
                        guessedWord: unsuccessfulGuess
                    }
                ]
            }

            const newState = store.getState()

            expect(newState).toEqual(expectedState)
        })
        test('should update state correctly for successful guess', () => {
            store.dispatch(guessWord(secretWord))
            const expectedState = {
                ...expectedDefaultState,
                ...initialState,
                success: true,
                newWordButtonVisible: true,
                guessedWords: [
                    {
                        letterMatchCount: 5,
                        guessedWord: secretWord
                    }
                ]
            }
            const newState = store.getState()
            expect(newState).toEqual(expectedState)
        })

    })
    describe('some guessed words', () => {
        const guessedWords = [{
            guessedWord: "agile",
            letterMatchCount: 1
        }]
        const initialState = { guessedWords, secretWord }
        let store
        beforeEach(() => {
            store = storeFactory(initialState)
        })
        test('should update state correctly for unsuccessful guess', () => {
            store.dispatch(guessWord(unsuccessfulGuess))
            const newState = store.getState();
            const expectedState = {
                ...expectedDefaultState,
                secretWord,
                success: false,
                guessedWords: [
                    ...guessedWords,
                    {
                        guessedWord: unsuccessfulGuess,
                        letterMatchCount: 3
                    }
                ]
            }
            expect(newState).toEqual(expectedState)
        })
        test('should update state correctly for successful guess', () => {
            store.dispatch(guessWord(secretWord))
            const newState = store.getState();
            const expectedState = {
                ...expectedDefaultState,
                secretWord,
                success: true, // show congrats message
                newWordButtonVisible: true, // show new game button
                guessedWords: [
                    ...guessedWords,
                    {
                        guessedWord: secretWord,
                        letterMatchCount: 5
                    }
                ]
            }
            // console.log('newState',newState);
            // console.log('expectedState',expectedState);
            expect(newState).toEqual(expectedState)
        })

    })

})

describe('resetGame action dispatcher', () => {
    const secretWord = 'party'
    const guessedWords = [{
        guessedWord: "party",
        letterMatchCount: 5
    }]
    const initialState = {
        ...expectedDefaultState,
        guessedWords,
        secretWord,
        success: true,
        newWordButtonVisible: true
    }
    beforeEach(() => {
        moxios.install()
    })
    afterEach(() => {
        moxios.uninstall()
    })

    test('reset game with new word', async () => {
        const store = storeFactory(initialState)
        moxios.wait(() => {
            const request = moxios.requests.mostRecent()
            request.respondWith({
                status: 200,
                response: "great"
            })
        })
        const expectedState = {
            ...expectedDefaultState,
            secretWord: "great",
        }

        // 'should get store state same as inital state before reset game action'
        expect(store.getState()).toEqual(initialState)

        // @ts-ignore
        return store.dispatch(resetGame()).then(() => {
            const newState = store.getState();
            expect(newState).toEqual(expectedState)
        })
    })
})


describe('update state when api call error', () => {
    beforeEach(() => {
        moxios.install()
    })
    afterEach(() => {
        moxios.uninstall()
    })

    test('should update state with error', () => {
        const store = storeFactory()

        moxios.wait(() => {
            const request = moxios.requests.mostRecent()
            request.respondWith({
                status: 500
            })
        })
        const expectedState = {
            ...expectedDefaultState,
            errors: true
        }
        // @ts-ignore
        return store.dispatch(getSecretWord()).then(() => {
            expect(expectedState).toEqual(store.getState())
        })
        // 'should get store state same as inital state before reset game action'
    })
})
