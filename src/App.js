// @ts-nocheck

import React, { useEffect } from "react"
import './App.css';
import Congrats from "./components/Congrats";
import GuessedWords from "./components/GuessedWords";
import Input from "./components/Input";
import TotalGuessed from "./components/TotalGuessed";
import { getSecretWord } from "./actions"
import { useDispatch, useSelector } from "react-redux"
import NewWordButton from "./components/NewWordButton/NewWordButton";
import GiveUpButton from "./components/GiveUpButton/GiveUpButton";
import ErrorComponent from "./components/ErrorComponent";

function App() {
  const success = useSelector(state => state.success);
  const guessedWords = useSelector(state => state.guessedWords);
  const secretWord = useSelector(state => state.secretWord);
  const errors = useSelector(state => state.errors);
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getSecretWord())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  // This dependency array is causing a compile warning because 'dispatch' should be in the array. But is causing App.test.get secret word. should not run on app update to fail because dispatch causes the useEffect to fire upon update. 
  // }, [dispatch])

  return (
    <div className="container" title="component-app">
      <h1>Jotto</h1>
      {errors ?
        <ErrorComponent /> :
        <div title="game-container">
          <Congrats success={success} />
          <NewWordButton />
          <GiveUpButton />
          <Input success={success} secretWord={secretWord} />
          <GuessedWords guessedWords={guessedWords} />
          <TotalGuessed />
        </div>
      }
    </div>
  );
}

export default App;
