import { getLetterMatchCount } from "./index";

describe('getLetterMatchCount', () => {
    const secretWord = 'party';

    test('should return correct count when there are no matching letters', () => {
        const letterGuessCount = getLetterMatchCount("bones", secretWord)
        expect(letterGuessCount).toBe(0)    
    })
    test('should return correct count when there are 3 matching letters', () => {        
        const letterGuessCount = getLetterMatchCount("train", secretWord)
        expect(letterGuessCount).toBe(3)    
    })
    test('should return currect count when there are duplicate letters in the guess', () => {
        const letterGuessCount = getLetterMatchCount("parka", secretWord)
        expect(letterGuessCount).toBe(3)    
    })
})

