/** 
* Collect the amount of matched letters between 2 strings and return the total
* @param {string} guessedWord - The user input from the text box
* @param {string} secretWord - The random generated word to compare guessedWord
* @returns {number} totalMatched letters
*/

export function getLetterMatchCount(guessedWord, secretWord){
    const secretLetters = secretWord.split("")
    const guesseLetterSet = new Set(guessedWord) 

    return secretLetters.filter(letter=> guesseLetterSet.has(letter)).length
}