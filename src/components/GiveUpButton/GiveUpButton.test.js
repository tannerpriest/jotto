import GiveUpButton from "./GiveUpButton";
import { Provider } from "react-redux";
import React from "react";
import { render } from "@testing-library/react";
import { storeFactory, queryByTitleAttr, newError } from "../../../test/testUtils"
jest.mock("../../actions")
import { giveUpAction as giveUpActionMock } from "../../actions";
import { fireEvent } from "@testing-library/dom";
import App from "../../App"

//                               RenderComponent will give you the option to render the giveupbutton or the whole app to check for rendering
const setup = (initalState = {}, RenderComponent = GiveUpButton) => {
    const store = storeFactory(initalState)

    return render(<Provider store={store}><RenderComponent /></Provider>)
}

/* TODO: BUILD COMPONENT TESTS, THEN BUILD INTEGRATION TEST, MAY NOT NEED TO MAKE NEW REDUCER: selector => state => state.guessedWords.length ? */

// render component
// - do not render button on load
// - render button after guess
// - show secret word after click

describe('render', () => {
    const componentWrapper = setup()

    const component = queryByTitleAttr(componentWrapper, "component-give-up")
    test('should render component', () => {
        expect(component).toBeTruthy()
    })

    describe('if no guesses', () => {
        test('should not render button if no guesses', () => {
            const wrapper = setup()
            if (component) {
                const button = queryByTitleAttr(wrapper, "give-up-button")
                return expect(button).toBeFalsy()
            }
            throw newError("Component never rendered to see if button child is hidden")
        })
    })

    describe('one guess', () => {
        let wrapper;
        beforeEach(() => {
            wrapper = setup({
                guessedWords: [
                    {
                        letterMatchCount: 3,
                        guessedWord: "party"
                    }
                ],
                secretWord: "hands"
            })
        })

        test('render button', () => {
            const button = queryByTitleAttr(wrapper, "give-up-button")
            expect(button).toBeTruthy()
        })
    })

    describe('if gaveUp is true', () => {
        let wrapper;
        beforeEach(() => {
            const state = {
                guessedWords: [
                    {
                        letterMatchCount: 3,
                        guessedWord: "agile"
                    }
                ],
                gaveUp: true,
                secretWord: "party"
            }
            wrapper = setup(state, App)
        })
        test('should render better luck message', () => {
            const betterLuckMessage = queryByTitleAttr(wrapper, "better-luck-message")
            expect(betterLuckMessage).toBeTruthy()
        })
        test('should render secret-word and match the redux state', () => {
            const secretWord = queryByTitleAttr(wrapper, "secret-word")
            expect(secretWord.innerHTML).toBe("party")
        })
    })
})

describe('should render component in App', () => {
    let wrapper;
    const startingState = {
        guessedWords: [
            {
                letterMatchCount: 3,
                guessedWord: "party"
            }
        ]
    }
    beforeEach(() => {
        wrapper = setup(startingState, App)
    })
    test('should render give up component', () => {
        const component = queryByTitleAttr(wrapper, "component-give-up")
        expect(component).toBeTruthy()
    })

})

describe('onClick behavior', () => {
    describe('run correct functions', () => {
        let wrapper
        beforeEach(() => {
            // @ts-ignore
            giveUpActionMock.mockClear()
            wrapper = setup({
                guessedWords: [
                    {
                        letterMatchCount: 3,
                        guessedWord: "party"
                    }
                ]
            }, App)
            const giveUpButton = queryByTitleAttr(wrapper, "give-up-button")
            fireEvent.click(giveUpButton);
        })

        test('should call setGaveUp useState', () => {
            expect(giveUpActionMock).toBeCalled()
        })
    })


    /** 
        I can not find any information on how to test useState and test its effect on the component. I am trying to 
        find out if the button that updates State to render 2 hidden components, render when state is set to true from false.
        May revisit later.
    */
    // describe('show new word button', () => {
    //     let wrapper;
    //     let originalUseState;
    //     // wrapper useState.gaveUp
    //     const startingState = {
    //         guessedWords: [
    //             {
    //                 letterMatchCount: 3,
    //                 guessedWord: "party"
    //             }
    //         ]
    //     }
    //     beforeEach(() => {
    //         wrapper = setup(startingState, App)
    //         originalUseState = React.useState
    //         mockSetGaveUp.mockClear()
    //     })
    //     afterEach(() => {
    //         React.useState = originalUseState
    //     })
    //     test('should render new word button after click give up', () => {
    //         const giveUpButton = queryByTitleAttr(wrapper, "give-up-button")
    //         fireEvent.click(giveUpButton);
    //         const newWordButton = queryByTitleAttr(wrapper, "new-game-button")
    //         expect(newWordButton).toBeTruthy()
    //     })
    //     test('should render better luck next time message', () => {
    //         const { rerender } = wrapper
    //         const button = queryByTitleAttr(wrapper, "give-up-button")
    //         fireEvent.click(button)
    //         const betterLuckMessage = queryByTitleAttr(wrapper, "better-luck-message")
    //         expect(betterLuckMessage).toBeTruthy()
    //     })
    // })

})
