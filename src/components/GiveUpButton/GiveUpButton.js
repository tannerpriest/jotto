import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { giveUpAction } from '../../actions'

const GiveUpButton = () => {
    // @ts-ignore
    const guessesLength = useSelector(state => state.guessedWords.length)
    // @ts-ignore
    const secretWord = useSelector(state => state.secretWord)
    // @ts-ignore
    const gaveUp = useSelector(state => state.gaveUp)

    const dispath = useDispatch()

    const handleGiveUp = () => {
       dispath(giveUpAction())
    }

    if (!guessesLength) return <div title="component-give-up" />

    return (
        <div title="component-give-up">
            {!gaveUp && <button onClick={handleGiveUp} title="give-up-button" className="btn btn-danger mb-2"> Give Up</button>}
            {gaveUp && <div>
                <h6 title="better-luck-message">Better luck next time</h6>
                <p title="secret-word">{secretWord}</p>
            </div>}
        </div>
    )
}

export default GiveUpButton
