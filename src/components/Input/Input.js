// @ts-nocheck
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'

import { guessWord } from "../../actions"

const Input = (props) => {
    const [currentGuess, setCurrentGuess] = useState("")
    const dispatch = useDispatch()
    const success = useSelector(state => state.success)
    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch(guessWord(currentGuess))
        setCurrentGuess("")
    }

    if (success) {
        return <div data-test="component-input" title="component-input" />
    }

    return (
        <div data-test="component-input" title="component-input">
            <form className="form-inline">
                <input
                    data-test="input-box"
                    title="input-box"
                    className="mb-2 mx-sm-3"
                    type="text"
                    placeholder="enter guess"
                    value={currentGuess}
                    onChange={(e) => setCurrentGuess(e.target.value)}
                />
                <button
                    data-test="submit-button"
                    title="submit-button"
                    className="btn btn-primary mb-2"
                    onClick={handleSubmit}
                >
                    Submit
                </button>
            </form>
        </div>
    )
}

Input.propTypes = {
    secretWord: PropTypes.string.isRequired,
    success: PropTypes.bool,
    setGuessedWords: PropTypes.func
}

export default Input
