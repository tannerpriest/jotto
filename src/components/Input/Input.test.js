import React from "react";
import { checkProps, queryByTitleAttr, storeFactory } from "../../../test/testUtils";
import { Provider } from "react-redux";
import Input from "./Input";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { fireEvent } from "@testing-library/dom";

let mockSetCurrentGuess = jest.fn();
jest.mock("react", () => ({
    // @ts-ignore
    ...jest.requireActual('react'),
    useState: (initialState) => [initialState, mockSetCurrentGuess]
}))

const setup = (initalState = {}, secretWord = 'party') => {
    const store = storeFactory(initalState)
    // return render(<Input secretWord={secretWord} success={success} setGuessedWords={() => { }} />)
    return render(<Provider store={store}><Input secretWord={secretWord} setGuessedWords={() => { }} /></Provider>)
}

describe('render', () => {
    describe('success is true', () => {
        let wrapper;
        beforeEach(() => {
            wrapper = setup({ success: true })
        })
        test('should render without error', () => {
            const component = queryByTitleAttr(wrapper, "component-input")
            expect(component).toBeTruthy()
        })
        test('should hide input box', () => {
            const inputBox = queryByTitleAttr(wrapper, "input-box")
            expect(inputBox).toBeFalsy()
        })
        test('should hide submit button', () => {
            const submitButton = queryByTitleAttr(wrapper, "submit-button")
            expect(submitButton).toBeFalsy()
        })
    })

    describe('success is false', () => {
        let wrapper;
        beforeEach(() => {
            wrapper = setup({ success: false })
        })
        test('should render without error', () => {
            const component = queryByTitleAttr(wrapper, "component-input")
            expect(component).toBeTruthy()
        })
        test('should show input box', () => {
            const inputBox = queryByTitleAttr(wrapper, "input-box")
            expect(inputBox).toBeTruthy()
        })
        test('should show submit button', () => {
            const submitButton = queryByTitleAttr(wrapper, "submit-button")
            expect(submitButton).toBeTruthy()
        })
    })

})



test('should not throw error with expected props', () => {
    checkProps(Input, { secretWord: "party" })
})

describe("state controlled input field", () => {
    let wrapper
    let originalUseState;
    beforeEach(() => {
        originalUseState = React.useState
        mockSetCurrentGuess.mockClear()
        wrapper = setup({ success: false })
    })
    afterEach(() => {
        React.useState = originalUseState
    })
    test('should update state with value of input box upon change', () => {
        const inputBox = queryByTitleAttr(wrapper, "input-box")


        // inputBox.simulate("change", mockEvent)

        /* The reason that I am using fireEvent as on "onChange" simulator, is that fireEvent and userEvent are similar, but have different work flows
            userEvent, under the hood uses fireEvent. 
            An example as provided by: https://stackoverflow.com/questions/64006345/react-testing-library-when-to-use-userevent-click-and-when-to-use-fireevent#:~:text=a%20good%20example%20is%20when%20you%20fire%20a%20click%20event%20on%20a%20button%20with%20fireEvent.click%2C%20the%20button%20will%20not%20be%20focused.%20The%20button%20is%20not%20focused.%20But%20with%20userEvent.click()%2C%20this%20button%20will%20be%20focused.%20useEvent%20can%20better%20reflect%20users%27%20real%20behaviour

            A fireEvent.change() event just fires the onChange event for the element and invokes the function tied to it. 
            A userEvent.type() however, give the element an actual user experience flow that invokes other events as well, like onFocus, onBlur and the userEvent bot literally types in 1 character at a time when performing the type event. Testing the component like it were a real user.

        */

        fireEvent.change(inputBox, { target: { value: "train" } })
        // userEvent.type(inputBox, "train")

        expect(mockSetCurrentGuess).toHaveBeenCalledWith("train")
        // expect(mockSetCurrentGuess).toHaveBeenCalledWith("a")
    })
    test('should clear input box upon submit button click', () => {
        const submitButton = queryByTitleAttr(wrapper, "submit-button")

        /** WHEN AN EVENT IS NEEDED FOR A BUTTON, you can pass an object in as a subsitute so that it doesnt not throw an error such as e.preventDefault() */

        userEvent.click(submitButton)

        expect(mockSetCurrentGuess).toHaveBeenCalledWith("")
    })

})
