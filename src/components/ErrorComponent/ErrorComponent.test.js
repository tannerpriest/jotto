import App from "../../App";
import { Provider } from "react-redux"
import ErrorComponent from "./ErrorComponent";
import { render } from "@testing-library/react";
import { storeFactory, queryByTitleAttr } from "../../../test/testUtils"

jest.mock("../../actions")

const setup = (initalState = {}, RenderComponent = ErrorComponent) => {
    const store = storeFactory(initalState)
    return render(<Provider store={store}><RenderComponent /></Provider>)
}

describe('render depending on errors', () => {

    describe('hidden if error is false', () => {
        const wrapper = setup({ errors: false })
        test('should not render message', () => {
            const component = queryByTitleAttr(wrapper, "error-message")
            expect(component).toBeFalsy()
        })
    })

    describe('if errors is true', () => {
        let wrapper;
        beforeEach(() => {
            wrapper = setup({ errors: true })
        })
        test('should render', () => {
            const component = queryByTitleAttr(wrapper, "error-component")
            expect(component).toBeTruthy()
        })
        test('should render', () => {
            const component = queryByTitleAttr(wrapper, "error-message")
            expect(component).toBeTruthy()
        })
    })
    describe('rendering in App', () => {
        let wrapper;
        beforeEach(() => {
            wrapper = setup({}, App)
        })
        test('should not render component', () => {
            const component = queryByTitleAttr(wrapper, "error-message")
            expect(component).toBeFalsy()
        })
    })
})

describe('based on state', () => {
    describe('if server throws error', () => {
        let wrapper
        beforeEach(() => {
            wrapper = setup({ errors: true }, App)
        })
        test('should show error message', () => {
            const component = queryByTitleAttr(wrapper, "error-message")
            expect(component).toBeTruthy()
        })
        test('should hide all other components', () => {
            const component = queryByTitleAttr(wrapper, "game-container")
            expect(component).toBeFalsy()
        })
    })

})
