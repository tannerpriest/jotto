import React from 'react'
import { useSelector } from 'react-redux'

const ErrorComponent = () => {
    //@ts-ignore
    const errors = useSelector(state => state.errors)
    if (!errors) return <div title="error-component" />
    return (
        <div title="error-component">
            <h5 className="h5-danger" title="error-message">There was an error connecting to the server. Please try again later</h5>
        </div>
    )
}

export default ErrorComponent
