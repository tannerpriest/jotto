import React from "react"
import { shallow } from "enzyme"
import { findByTestAttr, checkProps } from "../../../test/testUtils"
import GuessedWords from "./GuessedWords"

const defaultProps = {
    guessedWords: [{ guessedWord: "train", letterMatchCount: 3 }]
}

const setUp = (props = {}) => {
    const setUpProps = { ...defaultProps, ...props }
    return shallow(<GuessedWords {...setUpProps} />)
}

test('should not have warning with expected props', () => {
    checkProps(GuessedWords, defaultProps)
})


describe('if there are no words guessed', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = setUp({ guessedWords: [] })
    })

    test('should render without error', () => {
        const component = findByTestAttr(wrapper, "component-guessed-words")
        expect(component.length).toBe(1)
    })
    test('should renders instructions to guess word', () => {
        const component = findByTestAttr(wrapper, "component-instructions")
        expect(component.text().length).not.toBe(0)
    })
})

describe('if there are words guessed', () => {
    const guessedWords = [
        { guessedWord: "train", letterMatchCount: 3 },
        { guessedWord: "agile", letterMatchCount: 1 },
        { guessedWord: "party", letterMatchCount: 5 }
    ]

    let wrapper;
    beforeEach(() => {
        wrapper = setUp({ guessedWords })
    })
    test('should render without error', () => {
        const component = findByTestAttr(wrapper, "component-guessed-words")
        expect(component.length).toBe(1)
    })
    test('should render guessed words section', () => {
        const guessedWordNode = findByTestAttr(wrapper, "guessed-words")
        expect(guessedWordNode.length).toBe(1)
    })
    test('should display corredt number of guessed words', () => {
        const guessedWordNodes = findByTestAttr(wrapper, "guessed-word")
        expect(guessedWordNodes.length).toBe(3)
    })

})

describe('should show correct Guess number in guessed row', () => {
    const guessedWords = [
        { guessedWord: "train", letterMatchCount: 3 },
        { guessedWord: "agile", letterMatchCount: 1 },
        { guessedWord: "party", letterMatchCount: 5 }
    ]

    let wrapper;
    beforeEach(() => {
        wrapper = setUp({ guessedWords })
    })
    test('should have guess number of 1 for train', () => {
        const guessedWordsNodes = findByTestAttr(wrapper, "guessed-word")
        //           get all rows         <tr>   find the cells, (0) is the first column cell, get text
        const guessNums = guessedWordsNodes.map(row => row.find('td').at(0).text())
        const guessedWord = guessedWordsNodes.map(row => row.find('td').at(1).text())

        const expectedRowObject = {
            guessNums: "1",
            guessedWord: "train"
        }

        const renderedRow = {
            guessNums: guessNums[0],
            guessedWord: guessedWord[0]
        }
        expect(renderedRow).toEqual(expectedRowObject)
    })
    test('should have guess number of 3 for party', () => {
        const guessedWordsNodes = findByTestAttr(wrapper, "guessed-word")
        //           get all rows         <tr>   find the cells, (0) is the first column cell, get text
        const guessNums = guessedWordsNodes.map(row => row.find('td').at(0).text())
        const guessedWord = guessedWordsNodes.map(row => row.find('td').at(1).text())

        const expectedRowObject = {
            guessNums: "3",
            guessedWord: "party"
        }

        const renderedRow = {
            guessNums: guessNums[2],
            guessedWord: guessedWord[2]
        }
        expect(renderedRow).toEqual(expectedRowObject)
    })
})
