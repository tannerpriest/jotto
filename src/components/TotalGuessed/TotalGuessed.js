import React from 'react'
import { useSelector } from 'react-redux'

const TotalGuessed = (props) => {
    // @ts-ignore
    const guessedWordsTotal = useSelector(state => state.guessedWords.length)
    if (guessedWordsTotal === 0) {
        return <div title="total-guessed-component" />
    }
    return (
        <div title="total-guessed-component">
            <h3 title="total-guessed-message">
                Total Number of Guesses: <span title="total-guessed-number">{guessedWordsTotal}</span>
            </h3>
        </div>
    )
}

export default TotalGuessed
