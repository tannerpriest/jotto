import { Provider } from "react-redux";
import TotalGuessed from "./TotalGuessed";
import { render } from "@testing-library/react";
import { queryByTitleAttr, storeFactory } from "../../../test/testUtils";

jest.mock("../../actions")

const setup = (initialState = {
    secretWord: 'party',
    success: false,
    guessedWords: []
}) => {
    const store = storeFactory(initialState)
    const wrapper = render(<Provider store={store}><TotalGuessed /></Provider>)

    return wrapper
}

describe('rendering', () => {

    test('should render component', () => {
        const comp = setup()
        const getDiv = queryByTitleAttr(comp, "total-guessed-component")
        expect(getDiv).toBeTruthy()
    })
    

    test('should not render with no guessed', () => {
        const comp = setup()
        const getDiv = queryByTitleAttr(comp, "total-guessed-message")
        expect(getDiv).toBeFalsy()
    })
    
    test('should render with some guessed', () => {
        const initialState = {
            secretWord: 'party',
            success: false,
            guessedWords: [{ guessedWord: "agile", letterMatchCount: 1 }]
        }
        const comp = setup(initialState)
        const getDiv = queryByTitleAttr(comp, "total-guessed-message")
        expect(getDiv).toBeTruthy()
    })
})

describe('should show correct number of guesses', () => {

    describe('render total number span', () => {
        test('should render in h3', () => {
            const initialState = {
                secretWord: 'party',
                success: false,
                guessedWords: [{ guessedWord: "agile", letterMatchCount: 1 }]
            }
            const comp = setup(initialState)
            const getDiv = queryByTitleAttr(comp, "total-guessed-number")
            expect(getDiv).toBeTruthy()
        })
    })
    
    test('should render with some guessed', () => {
        const initialState = {
            secretWord: 'party',
            success: false,
            guessedWords: [{ guessedWord: "agile", letterMatchCount: 1 }]
        }
        const comp = setup(initialState)
        const getDiv = queryByTitleAttr(comp, "total-guessed-number")
        expect(getDiv.innerHTML).toBe("1")
    })
    test('should render with some guessed', () => {
        const initialState = {
            secretWord: 'party',
            success: false,
            guessedWords: [
                { guessedWord: "agile", letterMatchCount: 1 },
                { guessedWord: "train", letterMatchCount: 3 },
                { guessedWord: "weird", letterMatchCount: 1 }
            ]
        }
        const comp = setup(initialState)
        const getDiv = queryByTitleAttr(comp, "total-guessed-number")
        expect(getDiv.innerHTML).toBe("3")
    })
})
