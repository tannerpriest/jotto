import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { resetGame } from '../../actions'

const NewWordButton = () => {
    // @ts-ignore
    const newWordButtonVisible = useSelector(state => state.newWordButtonVisible)
    const dispatch = useDispatch()

    const handleNewGame = (e) => {
        e.stopPropagation();
        dispatch(resetGame())
    }

    if (!newWordButtonVisible) return <div title="component-new-game-button" />

    return (
        <div title="component-new-game-button">
            <button
                title="new-game-button"
                className="btn btn-success mb-2"
                onClick={handleNewGame}
            >
                New Word
            </button>
        </div>
    )
}

export default NewWordButton
