import NewWordButton from "./NewWordButton";
import { Provider } from "react-redux"
import { render } from "@testing-library/react";
import { storeFactory, queryByTitleAttr } from "../../../test/testUtils"
jest.mock("../../actions")
import { resetGame as resetGameMock } from "../../actions";
import { fireEvent } from "@testing-library/dom";
import App from "../../App";

const setup = (initalState = {}, RenderComponent = NewWordButton) => {
    const store = storeFactory(initalState)
    return render(<Provider store={store}><RenderComponent /></Provider>)
}

describe('render depending on success', () => {

    describe('if visible is false', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = setup({ newWordButtonVisible: false })
        })
        test('should render component', () => {
            const component = queryByTitleAttr(wrapper, "component-new-game-button")
            expect(component).toBeTruthy()
        })
        test('should hide button if state is false', () => {
            const button = queryByTitleAttr(wrapper, "new-game-button")
            expect(button).toBeFalsy()
        })
    })

    describe('if visible is true', () => {
        let wrapper;

        beforeEach(() => {
            wrapper = setup({ newWordButtonVisible: true })
        })
        test('should show button', () => {
            const component = queryByTitleAttr(wrapper, "new-game-button")
            expect(component).toBeTruthy()
        })

    })
})

describe('should call resetGame when clicked', () => {
    let wrapper;
    beforeEach(() => {
        // @ts-ignore
        resetGameMock.mockClear()
        wrapper = setup({ newWordButtonVisible: true })
    })
    test('should run resetGame once when clicked', () => {
        const button = queryByTitleAttr(wrapper, "new-game-button")
        
        fireEvent.click(button, { stopPropagation: () => {} })

        expect(resetGameMock).toBeCalled()
    })
})

describe('should render and behave in App', () => {
    let wrapper;
    beforeEach(() => {
        // @ts-ignore
        resetGameMock.mockClear()
        wrapper = setup({ newWordButtonVisible: true }, App)
    })
    test('should render component', () => {
        const component = queryByTitleAttr(wrapper, "component-new-game-button")
        expect(component).toBeTruthy()
    })
    test('should run resetGame once when clicked', () => {
        const button = queryByTitleAttr(wrapper, "new-game-button")
        
        fireEvent.click(button, { stopPropagation: () => {} })

        expect(resetGameMock).toBeCalled()
    })
})

