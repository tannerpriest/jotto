import { shallow, ShallowWrapper } from "enzyme"
import { findByTestAttr, checkProps } from "../../../test/testUtils";

import Congrats from "./Congrats";

const defaultCongratsProps = { success: false }

/** 
* Takes props and render Congrats component
* @param {object} props object for shallow component 
* @returns {ShallowWrapper} Shallow Congrats component for JEST to test 
*/
const setup = (props = {}) => {
    const setUpProps = {...defaultCongratsProps, ...props}
    return shallow(<Congrats {...setUpProps} {...props} />)
}

test('renders without error', () => {
    const wrapper = setup({ success: false })
    const component = findByTestAttr(wrapper, "component-congrats")
    expect(component.length).toBe(1)
});

test('should not text when success if false', () => {
    const wrapper = setup({ success: false })
    const component = findByTestAttr(wrapper, "component-congrats")
    expect(component.text()).toBe("")
})

test('should render non-empty congrats message when success if true', () => {
    const wrapper = setup({ success: true })
    const message = findByTestAttr(wrapper, "congrats-message")
    expect(message.text().length).not.toBe(0)
})

test('should not throw warning with expected props', () => {
    const expectedProps = { success: false }
    checkProps(Congrats, expectedProps)
})

