import React from 'react'
import PropTypes from "prop-types"

/**
 * 
 * @param {object} props - React props
 * @returns {JSX.Element} - Rendered component (or null if 'success' prop if false)
 */

const Congrats = ({ success }) => {
    if (success) {
        return (<div data-test="component-congrats" title="component-congrats" className="alert alert-success">
            <span data-test="congrats-message" title="congrats-message">
                You guessed the word
            </span>
        </div>
        )
    }
    return <div data-test="component-congrats" title="component-congrats" />
}

Congrats.propTypes = {
    success: PropTypes.bool.isRequired
}

export default Congrats

// receive the success start as a prop
