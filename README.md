# Challenges 

  ### #1: Number of Guesses - ☑️
 
- Add "guess number" column to the "Guessed Words" table
- Add a component that displays total guesses 
- Use only existing state
 
### #2: Reset Game - ☑️ 
 
- Add a "New Word Button"
    - Display after successful guess
- Reset games with a new word from server
- Get state from useSelector or props
 
### #3: Give Up Button - ☑️ 
- Display give up button after at least 1 failed guess
- Show secret word
- If clicked, display secret word in Failed message pop up and show new word button.
 
### #4: User Inputs secret word
- Display "Enter your own secret word" button
- User inputs a 5 letter word
- Have button to submit new word
- start the game with the inputed word
 
### #5: Random Word Server Error display - ☑️ 
- Display error if there is a problem with the server
- If there is a problem contacting the server
- If server responds with an error 5xx or 4xx status code
 
### #6: Use Wordnik to get secret word
- get wordnik auth token 
- http://developer.wordnik.com/
- Use words/randomWord endpoint
- http://developer.wordnik.com/docs.html

