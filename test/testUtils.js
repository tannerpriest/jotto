import { ShallowWrapper } from "enzyme"
import checkPropTypes from "check-prop-types"
import { RenderResult, MatcherOptions, waitForOptions } from "@testing-library/react"
import { createStore, Store, applyMiddleware } from "redux"
import { middleWares } from "../src/configureStore"
import rootReducer, { expectedDefaultState } from "../src/reducers"

/** 
*  Create testing store with imported reducers, middleware and initial state  
* @param {object} initalState -  ArgumentDescriptions 
* @returns {Store} 
*/

export const storeFactory = (initalState) => createStore(rootReducer, { ...expectedDefaultState, ...initalState }, applyMiddleware(...middleWares))

/** 
* findByTestAttr Return Nodes with given data-test attributes
* @param {ShallowWrapper} wrapper - Enzyme Wrapper
* @param {string} val - Enzyme Wrapper
* @returns {ShallowWrapper} What function returns 
*/

export const findByTestAttr = (wrapper, val) => {
    return wrapper.find(`[data-test="${val}"]`)
}

export const checkProps = (component, conformingProps) => {
    // @ts-ignore
    const propsError = checkPropTypes(
        component.propTypes,
        conformingProps,
        "prop",
        component.name
    )
    expect(propsError).toBeUndefined()
}

/** 
* Return node from testing Library render function 
* @param {RenderResult<typeof import("/Users/tannerpriest/Projects/jotto/node_modules/@testing-library/dom/types/queries"), HTMLElement>} component - The main component to Render
* @param {string} title - The title title to find the node inside of the component
* @param {MatcherOptions} options - The title title to find the node inside of the component
* @returns {HTMLElement} 
*/

export const queryByTitleAttr = (component, title, options = {}) => component.queryByTitle(title, options)

/** 
* Return multiple nodes from testing Library render function 
* @param {RenderResult<typeof import("/Users/tannerpriest/Projects/jotto/node_modules/@testing-library/dom/types/queries"), HTMLElement>} component - The main component to Render
* @param {string} title - The title title to find the node inside of the component
* @param {MatcherOptions} options - The title title to find the node inside of the component
* @param {waitForOptions} options - The title title to find the node inside of the component
* @returns {HTMLElement[]} 
*/

export const queryAllTitleAttr = (component, title, options = {}, waitOptions = {}) => component.queryAllByTitle(title, options, waitOptions)

export const newError = (message) => new Error(message)
